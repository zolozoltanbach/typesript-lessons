import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // ez az inicializacios resz
  title = 'lesson'; 
  title2 = 'lesson2';
  color = 'piros';

  // ez egy metodus ami a gomb megnyomasra fut le
  modify () {
    this.title = 'mission';
    this.color = 'nagy';
  }
}

